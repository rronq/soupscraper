# SoupScraper

This will be a collection of Python scripts written using BeautifulSoup4. I created these to learn about web scraping as well as utilize them in my daily life. Over time, I will probably improve or combine scripts for better and more efficient functionality.
**Note: You will have to install beautifulsoup4 and lxml to run these scripts.**
## soupnews.py
I wrote this script to pull down data from https://news.ycombinator.com/ since I go here quite often. The output from the script is written to a csv file.