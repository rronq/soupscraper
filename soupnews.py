from bs4 import BeautifulSoup
import requests
import csv
import time


def scraper(url):
    source = requests.get(url).text
    soup = BeautifulSoup(source, 'lxml')
    csv_file = open('output.csv', 'w')
    csv_write = csv.writer(csv_file)
    csv_write.writerow(['Rank','Title','Link'])

    for table in soup.find_all(class_='athing'):
        rank = table.find(class_="rank").get_text()
        print(rank)
        title = table.find(class_="storylink").get_text()
        print(title)
        link = table.find('a',{'class':'storylink'})['href']
        print(link)
        time.sleep(2)

        print()
        csv_write.writerow([rank,title,link])
    
    csv_file.close()                                  
scraper('https://news.ycombinator.com/')